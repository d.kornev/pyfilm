from flask import Flask

from flask_uploads import (UploadSet, patch_request_class,
                           configure_uploads, IMAGES)
from flask_mongoengine import MongoEngine
from flask_login import LoginManager

from config import config


db = MongoEngine()
login_manager = LoginManager()

photos = UploadSet('photos', IMAGES)


def create_app(config_name):
    app = Flask(__name__)

    app.config.from_object(config[config_name])

    db.init_app(app)
    login_manager.init_app(app)

    configure_uploads(app, photos)
    patch_request_class(app, 3 * 1024 * 1024)

    from .user import user as user_blueprint
    from .blog import blog as blog_blueprint
    from .upload import upload as upload_blueprint
    from .errors import errors as errors_blueprint

    app.register_blueprint(user_blueprint, url_prefix='/user')
    app.register_blueprint(blog_blueprint, url_prefix='/blog')
    app.register_blueprint(upload_blueprint, url_prefix='/upload')
    app.register_blueprint(errors_blueprint)

    if config_name == 'development':
        @app.after_request
        def cors_for_development(response):
            response.headers['Access-Control-Allow-Origin'] = '*'
            response.headers['Access-Control-Allow-Methods'] = 'POST,GET,OPTIONS,HEAD,PUT,DELETE'
            response.headers['Access-Control-Allow-Headers'] = 'Content-Type,Accept,Authorization'
            return response

    return app
