import bbcode


parser = bbcode.Parser()
parser.add_simple_formatter('youtube',
                            '''
                            <div class="embed-responsive embed-responsive-16by9">
                            <iframe class="embed-responsive-item"
                            src="https://www.youtube.com/embed/%(value)s"
                            allowfullscreen></iframe>
                            </div>
                            ''')
parser.add_simple_formatter('img',
                            '<img class="img-responsive thumbnail" src="%(value)s">', replace_links=False)


def generateHtml(value):
    return parser.format(value)
