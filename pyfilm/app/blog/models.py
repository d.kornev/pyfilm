from app import db


from bson.objectid import ObjectId
import datetime


class Comment(db.EmbeddedDocument):
    comment_id = db.ObjectIdField(primary_key=True, default=ObjectId)
    username = db.StringField(required=True, min_length=2, max_length=20)
    content = db.StringField(required=True, min_length=3, max_length=120)
    published = db.DateTimeField(default=datetime.datetime.now)
    active = db.BooleanField(default=True)


class Post(db.Document):
    title = db.StringField(required=True, min_length=3, max_length=50)
    published = db.DateTimeField(default=datetime.datetime.now().date)
    slug = db.StringField(required=True, min_length=3,
                          max_length=50, unique_with='published')
    content = db.StringField()
    content_bbcode = db.StringField(required=True, min_length=3)
    author = db.StringField(required=True, max_length=20)
    tags = db.SortedListField(db.StringField(max_length=20), default=list)
    comments = db.EmbeddedDocumentListField(Comment)
    total_comments = db.IntField(default=0)
    total_views = db.IntField(default=0)
