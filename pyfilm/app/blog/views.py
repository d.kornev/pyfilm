from flask import jsonify, request, abort, url_for

from mongoengine.queryset.visitor import Q
from slugify import slugify

from . import blog
from app import db

from .models import Post, Comment

from flask_login import login_required, current_user
from datetime import datetime

from .helpers import generateHtml


@blog.route('/post')
def list():
    page = request.args.get('page', 1, type=int)
    tag = request.args.get('tag', None, type=str)

    query = Post.objects

    if tag:
        query = query(tags=tag)

    posts = query.exclude('comments', 'content_bbcode')\
        .order_by('-id').paginate(page=page, per_page=3)

    data_json = {}
    data_json['data'] = [post for post in posts.items]

    if posts.has_next:
        url_args = {
            "page": posts.next_num,
            "_external": True
        }
        if tag:
            url_args["tag"] = tag
        next_page = url_for('.list', **url_args)
    else:
        next_page = False

    data_json['next_page'] = next_page
    # posts = Post.objects.aggregate({
    #     "$project":
    #     {"title": True, "content": True, "author": True,
    #      "total": {"$size": "$comments"}}})

    return jsonify(data_json)


@blog.route('/post/<date>/<slug>')
def detail(date, slug):
    try:
        date = datetime.strptime(date, '%Y-%m-%d')
    except:
        abort(500)

    post = Post.objects.get_or_404(Q(published__gte=date) & Q(slug=slug))
    post.update(inc__total_views=1)
    return jsonify(post)


@blog.route('/post', methods=['POST'])
@login_required
def create():
    try:
        req = request.json

        post = Post()
        post.title = req['title']
        post.slug = slugify(post.title)
        post.content = generateHtml(req['content'])
        post.content_bbcode = req['content']
        post.tags = req['tags']
        post.author = current_user.username

        post.save()

        date = datetime.strftime(post.published, '%Y-%m-%d')
        location = url_for('.detail', date=date,
                           slug=post.slug, _external=True)

        response = (jsonify({'message': 'created'}),
                    201, {'Location': location})

        return response
    except db.ValidationError as e:
        return jsonify(e.to_dict()), 422
    except:
        abort(500)


@blog.route('/post/<id>', methods=['PUT'])
@login_required
def update(id):
    try:
        req = request.json

        post = Post.objects(id=id).get_or_404()
        post.title = req['title']
        post.content_bbcode= req['content']
        post.content = generateHtml(req['content'])
        post.tags = req['tags']
        post.author = current_user.username

        post.save()

        date = datetime.strftime(post.published, '%Y-%m-%d')

        location = url_for('.detail', date=date, slug=post.slug, _external=True)

        response = (jsonify({'message': 'updated'}),
                    201, {'Location': location})

        return response
    except:
        abort(500)


@blog.route('/post/<id>', methods=['DELETE'])
@login_required
def delete(id):
    try:
        Post.objects(id=id).get_or_404().delete()
        return jsonify({"message": "deleted"}), 204
    except:
        abort(500)


@blog.route('/post/<id>/comment', methods=['POST'])
def add_comment(id):
    try:
        req = request.json
        username = req['username']
        content = req['content']

        comment = Comment(username=username, content=content)

        Post.objects(id=id).update_one(
            push__comments=comment,
            inc__total_comments=1)

        return jsonify(comment), 201
    except db.ValidationError as e:
        return jsonify(e.to_dict()), 422
    except:
        abort(500)


@blog.route('/post/<post_id>/comment/<comment_id>', methods=['DELETE'])
@login_required
def remove_comment(post_id, comment_id):
    try:
        Post.objects(id=post_id).update_one(
            pull__comments__comment_id=comment_id,
            dec__total_comments=1)
        return jsonify({'message': 'deleted'}), 200
    except:
        abort(500)
