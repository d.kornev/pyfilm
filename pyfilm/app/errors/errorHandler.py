from flask import jsonify
from . import errors


@errors.app_errorhandler(401)
def unauthorized(e):
    return jsonify({"message": "unauthorized"}), 401


@errors.app_errorhandler(403)
def forbidden(e):
    return jsonify({"message": "forbidden"}), 403


@errors.app_errorhandler(404)
def page_not_found(e):
    return jsonify({"message": "page not found"}), 404


@errors.app_errorhandler(405)
def method_not_allowed(e):
    return jsonify({"message": "method not allowed"}), 405


@errors.app_errorhandler(413)
def entity_too_large(e):
    return jsonify({"message":
                    "The data value transmitted exceeds the capacity limit"
                    }), 413


@errors.app_errorhandler(500)
def internal_server_error(e):
    return jsonify({"message": "internal server error"}), 500
