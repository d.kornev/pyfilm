from app import db

from app.user.models import User


class File(db.Document):
    path = db.StringField(required=True)
    owner = db.ReferenceField(User)
