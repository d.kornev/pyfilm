from flask import jsonify, request, url_for
from flask_login import login_required, current_user

from slugify import slugify

from app import photos
from .models import File

from . import upload


@upload.route('/')
@login_required
def list():
    page = request.args.get('page', 1, type=int)

    all_files = File.objects(owner=current_user.id)\
        .exclude('owner').order_by('-id').paginate(page, per_page=5)

    data_json = {}
    data_json['data'] = [files for files in all_files.items]

    prev_page = False
    next_page = False

    if all_files.has_prev:
        prev_page = url_for('.list',
                            page=all_files.prev_num, _external=True)

    if all_files.has_next:
        next_page = url_for('.list',
                            page=all_files.next_num, _external=True)

    data_json['prev_page'] = prev_page
    data_json['next_page'] = next_page

    return jsonify(data_json)


@upload.route('/', methods=['POST'])
@login_required
def file_upload():
    if 'image' in request.files:
        filename = photos.save(request.files['image'],
                               folder=slugify(current_user.username))
        f = File(path=filename, owner=current_user._get_current_object())
        f.save()
        return jsonify(f)


@upload.route('/<id>', methods=['DELETE'])
@login_required
def file_delete(id):
    pass
