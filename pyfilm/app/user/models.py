from app import db, login_manager

from flask_login import UserMixin
from werkzeug.security import generate_password_hash, check_password_hash

import os
import binascii


class User(UserMixin, db.Document):

    username = db.StringField(unique=True, required=True)
    password = db.StringField(required=True)
    token = db.StringField(unique=True, default=lambda:
                           binascii.hexlify(os.urandom(25)).decode())

    def set_password(self, password):
        self.password = generate_password_hash(password)

    def verify_hash(self, password):
        return check_password_hash(self.password, password)
    # def clean(self):
    #     if not self.token:
    #         self.token = binascii.hexlify(os.urandom(25)).decode()


@login_manager.request_loader
def load_user(request):
    # len(Token) == 50
    token = request.headers.get('Authorization')
    if token:
        token = token.replace('Bearer ', '', 1)
        if len(token) == 50:
            try:
                user = User.objects(token=token)[0]
                if user:
                    return user
            except:
                return None
    return None
