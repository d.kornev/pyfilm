from flask import request, abort, jsonify

from . import user

from .models import User


@user.route('/token', methods=['POST'])
def token():
    try:
        req = request.json
        username = req['username']
        password = req['password']
        user = User.objects(username=username)[0]
        if user.verify_hash(password):
            return jsonify(token=user.token)
        else:
            abort(401)
    except:
        abort(401)
