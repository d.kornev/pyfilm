import os


class Config:
    SECRET_KEY = os.environ.get('SECRET_KEY', 'jtrtjltrjt90209290KJZJaiojroiai1zzza:ar')
    SERVER_NAME = os.environ.get('PYFILM_SERVER_NAME', '127.0.0.1:5000')

    UPLOADED_PHOTOS_DEST = os.path.join('static', 'images')


class ProductionConfig(Config):
    MONGODB_DB = 'pyfilm'
    MONGODB_HOST = os.environ.get('PYFILM_DB_HOST', '127.0.0.1')
    MONGODB_USERNAME = os.environ.get('PYFILM_DB_USERNAME', '')
    MONGODB_PASSWORD = os.environ.get('PYFILM_DB_PASSWORD', '')

class DevelopmentConfig(Config):
    DEBUG = True
    MONGODB_DB = 'pyfilm_dev'
    # MONGODB_HOST = '192.168.122.101'
    MONGODB_HOST = '10.0.3.50'


class TestingConfig(Config):
    MONGODB_DB = 'pyfilm_test'
    MONGODB_HOST = '10.0.3.50'

config = {
    'development': DevelopmentConfig,
    'production': ProductionConfig,
    'testing': TestingConfig
}
