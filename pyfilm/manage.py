import os


current_dir = os.path.dirname(os.path.realpath(__file__))
env_config = os.path.join(current_dir, 'env')

if os.path.exists(env_config):
    print('Importing environment from env...')
    for line in open(env_config):
        var = line.strip().split('=')
        if len(var) == 2:
            os.environ[var[0]] = var[1]


from flask_script import Manager, Shell
import unittest

from app import create_app, db
from app.user.models import User
from app.blog.models import Post, Comment

from faker import Faker
from slugify import slugify
from app.blog.helpers import generateHtml

import random
from getpass import getpass


app = create_app(os.environ.get('FLASK_CONFIG', 'development'))
manager = Manager(app)


def make_context():
    return dict(app=app, db=db, User=User, Post=Post, Comment=Comment)


@manager.command
def createsuperuser():

    username = input('Enter username: ')
    password = getpass('Enter password: ')
    password_confirm = getpass('Confirm password: ')

    if password == password_confirm:
        user = User(username=username)
        user.set_password(password)
        try:
            user.save()
            print('user %s has been created' % (username))
        except:
            print('User already exist')
    else:
        print('Error: passwords does not match')

@manager.command
def fakedb():

    total_records = 50

    faker = Faker()

    tags_list = faker.words(nb=14)

    admins = [faker.last_name() for _ in range(3)]

    posts = []

    for _ in range(total_records):
        rn = random.randint(1, 4)
        tags = []
        for _a in range(1, rn):
            tags.append(random.choice(tags_list))

        comms = []
        for _b in range(1, random.randint(1, 15)):
            comms.append(Comment(username=faker.word(),
                                 content=faker.text(max_nb_chars=120)))

        title = ' '.join(faker.words(nb=3))
        slug = slugify(title)
        published = faker.date_time_this_year(before_now=True)
        content_bbcode = faker.text(max_nb_chars=12000)
        content = generateHtml(content_bbcode)
        author = random.choice(admins)
        t_coms = len(comms)
        t_views = random.randint(0, 400)
        posts.append(Post(title=title, slug=slug, published=published,
                          content_bbcode=content_bbcode, content=content,
                          comments=comms, total_comments=t_coms,
                          total_views=t_views, tags=tags, author=author))

    Post.objects.insert(posts)


@manager.command
def test():
    tests = unittest.TestLoader().discover('tests', pattern='*test.py')
    unittest.TextTestRunner(verbosity=2).run(tests)

manager.add_command('shell', Shell(make_context=make_context))

if __name__ == '__main__':
    manager.run()
