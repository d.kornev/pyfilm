import unittest

from app import create_app
from app.user.models import User


class BasicTestCase(unittest.TestCase):

    user = {
        'username': 'admin',
        'password': 'qwerty'
    }

    def setUp(self):

        User.drop_collection()

        self.app = create_app('testing')
        self.app_context = self.app.app_context()
        self.app_context.push()
        u = User(username=self.user['username'])
        u.set_password(self.user['password'])
        u.save()
        self.client = self.app.test_client()

    def tearDown(self):
        User.drop_collection()
        self.app_context.pop()
