import json

from flask import url_for

from app.blog.models import Post
from app.user.models import User

from basicTestCase import BasicTestCase


class BlogCase(BasicTestCase):
    default_post = {
        "title": "test title",
        "slug": "test slug",
        "content": "test content",
        "content_bbcode": "test content",
        "tags": ["test tag1"],
        "author": "basic man"
    }
    admin_post = {
        "title": "admin title",
        "content": "admin content",
        "tags": ["admin tag"],
        "author": "admin"
    }

    def setUp(self):
        super(BlogCase, self).setUp()
        Post(**self.default_post).save()

    def tearDown(self):
        super(BlogCase, self).tearDown()
        Post.drop_collection()

    def test_anonymous_can_see_list_view(self):
        response = self.client.get(url_for('blog.list'))
        data = json.loads(response.data.decode('utf-8'))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(data['data'][0]['title'], self.default_post['title'])

    def test_admin_can_create_post(self):
        user = User.objects()[0]
        headers = {'Authorization': 'Bearer '+user.token,
                   'Content-Type': 'application/json'}

        response = self.client.post(url_for('blog.create'),
                                    data=json.dumps(self.admin_post),
                                    headers=headers)

        post = Post.objects(title=self.admin_post['title']).first()

        self.assertEqual(response.status_code, 201)
        self.assertEqual(post.title, self.admin_post['title'])
