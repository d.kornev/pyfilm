import json

from flask import url_for
from app.user.models import User

from basicTestCase import BasicTestCase


class AuthCase(BasicTestCase):

    def test_default_user(self):
        u = User.objects[0]
        self.assertEqual(u.username, 'admin')

    def test_get_token(self):
        response = self.client.post(url_for('user.token'),
                                    data=json.dumps(self.user),
                                    content_type='application/json')

        data = json.loads(response.data.decode('utf-8'))
        user = User.objects(username=self.user['username'])[0]
        self.assertEqual(response.status_code, 200)
        self.assertEqual(data['token'], user.token)
